from django.shortcuts import render
from lab_1.views import mhs_name, birth_date

#TODO Implement
#Create a content paragraph for your landing page:
landing_page_content = 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nam egestas augue vel feugiat ultricies. Pellentesque pulvinar, ligula et euismod aliquam, nibh urna vulputate ex, pretium eleifend dui enim sit amet.'

def index(request):
    response = {'name': mhs_name, 'content': landing_page_content}
    return render(request, 'index_lab2.html', response)
